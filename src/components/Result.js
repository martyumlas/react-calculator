import React, { Component } from 'react'

export class Result extends Component {
    render() {
        let result = this.props.result
        let memoryStatus = this.props.memoryStatus
        return (
            <div className='result'>
                <p className='result-output'>{result}</p>
                <p>{memoryStatus}</p>
            </div>
        )
    }
}

export default Result
