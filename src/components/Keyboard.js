import React, { Component } from 'react'

export class Keyboard extends Component {
    render() {
        let memory = ['MC', 'MR', 'M+', 'M-'];
        let keys = ['7', '8', '9' ,'/', '4', '5', '6', '*', '1', '2','3', '-', '0', '.', '+', '='];

        const memoryButtons = memory.map((item) => {
            return <button key={item} name={item} onClick={e => this.props.handleMemory(e.target.name)}>{item}</button>
        })
        
        const keyButtons = keys.map((key) => {
            return <button name={key} key={key} onClick={e => this.props.onClick(e.target.name)}>{key}</button>
        })


        return (
            <div className="keyboard"> 
                {memoryButtons }

                <button className='item1 material-icons' name="DEL" onClick={e => this.props.onClick(e.target.name)}>
                    backspace
                </button>              
                <button className='item2' name="C" onClick={e => this.props.onClick(e.target.name)}>C</button> 
                 
                {keyButtons}                         
            </div>
        )
    }
}

export default Keyboard
