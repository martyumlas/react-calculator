import React, { Component } from 'react'
import Result from './components/Result'
import Keyboard from './components/Keyboard'
import './App.css'

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      result: "",
      memoryStatus: '',
      memoryValue : '',
    }
  }
  onClick = (payload) => {  
     switch (payload) {
       case 'C':
         this.reset();
         break;
       case 'DEL':
         this.backspace();
         break;
       case '=':
         this.calculate();
         break;
       default:
         this.setState({
           result: this.state.result + payload
         })

     }
  }
  handleMemory = (payload) => {
    let {result, memoryValue, memoryStatus} = this.state
    switch (payload) {
      case 'M+':
        if (result !== '' && memoryStatus === '') {
          this.setState({
            memoryStatus: 'M',
            memoryValue: this.state.result,
          })

        }
        break;
      case 'M-':
        if (memoryStatus) {
          this.setState({
            memoryStatus: '',
            memoryValue: ''
          })
        }
        break;
      case 'MR':
        if (memoryStatus) {
          this.setState({
            result : memoryValue
          })
        }
        break;
      case 'MC':
        if (memoryStatus) {
          this.setState({
            memoryStatus: '',
            memoryValue: ''
          })
        }
        break; 
      default:
      break;
    }
     
  }
  reset = () => {
    this.setState({
      result: ""
    })
  };
  backspace = () => {
    this.setState({
      result: this.state.result.slice(0, -1)
    })
  };
  calculate = () => {
   
    try {
      this.setState({
        // eslint-disable-next-line
        result: (eval(this.state.result) || "") + "",   
      })
      
     
     
    } catch (e) {
      this.setState({
        result: "error"
      })

    }
  };
  render() {
    return (
      <div className='calculator-container'>
        <div className="calculator">
          <Result result={this.state.result} memoryStatus={this.state.memoryStatus}/>
          <Keyboard onClick={this.onClick} handleMemory={this.handleMemory}/>
        </div>
      </div>
    )
  }
}

export default App
